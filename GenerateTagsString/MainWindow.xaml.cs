﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive.Linq;
using System.Runtime.InteropServices;
using System.Windows;
using ReactiveUI;
using ReactiveUI.Xaml;

namespace GenerateTagsString
{
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new ViewModel();
        }
    }


    public class ViewTagModel : ReactiveObject
    {
        private readonly ObservableCollection<ViewTagModel> _ancestors;
        private readonly IEnumerable<string> _defaultRanks = new[] {"Both", "Column", "Row"};
        private readonly Guid _id;
        private ViewTagModel _Ancestor;
        private string _Name;
        private string _Rank;
        private ObservableAsPropertyHelper<IEnumerable<string>> _Ranks;
        private ObservableAsPropertyHelper<IEnumerable<string>> _SiteItems;
        private string _Siting;

        private ViewTagModel()
        {
        }

        public static ViewTagModel Create(string[] strings,ViewTagModel[] tags )
        {
            if (strings.Count()<4)
                throw new InvalidOleVariantTypeException(string.Format("strings count error: {0}",strings.Count()));
            
            return new ViewTagModel(tags.ToArray(),strings[1].ToLower() == "null" ? null : tags.SingleOrDefault(t => t.Name == strings[1]))
                    {
                        Name = strings[0],
                            
                        Siting = strings[2],
                        Rank = strings[3]
                    };
            
        }
        public ViewTagModel(IEnumerable<ViewTagModel> tags,ViewTagModel ancestor=null)
        {
            _id = Guid.NewGuid();
            
            if (ancestor == null)
                ancestor = new ViewTagModel();
            
            _Ancestor = ancestor;
            
            _ancestors = new ObservableCollection<ViewTagModel>();
            if (tags != null)
            {
                _ancestors.Add(new ViewTagModel());
                foreach (ViewTagModel tag in tags.TakeWhile(tag => tag.Name != Name))
                {
                    Ancestors.Add(tag);
                }
            }
            //this.WhenAny(x => x.Ancestor, ancetor => ancetor.Value)
            //    .Where(ancestor => ancestor != null)
            //    .Select(an=>an)
            Ancestor.WhenAny(o => o.Rank, rank => {
                {
                    var temp = DecideRanks(rank.Value) ;
                    var ranks = temp as string[] ?? temp.ToArray();
                    if (ranks.Count() == 1)
                        Rank = ranks.FirstOrDefault();

                    return ranks;
                } })
                .ToProperty(this, o => o.Ranks);

            this.WhenAny(o => o.Rank, rank => DecideSiteItems(rank.Value))
                .ToProperty(this, x => x.SiteItems);
        }

        public Guid Id
        {
            get { return _id; }
        }

        public string Name
        {
            get { return _Name; }
            set { this.RaiseAndSetIfChanged(x => x.Name, value); }
        }

        public ViewTagModel Ancestor
        {
            get { return _Ancestor; }
            set { this.RaiseAndSetIfChanged(x => x.Ancestor, value); }
        }

        public string Siting
        {
            get { return _Siting; }
            set { this.RaiseAndSetIfChanged(x => x.Siting, value); }
        }

        public string Rank
        {
            get { return _Rank; }
            set { this.RaiseAndSetIfChanged(x => x.Rank, value); }
        }

        public ObservableCollection<ViewTagModel> Ancestors
        {
            get { return _ancestors; }
        }

        public IEnumerable<string> Ranks
        {
            get { return _Ranks.Value; }
        }

        public IEnumerable<string> SiteItems
        {
            get { return _SiteItems.Value; }
        }

        public override string ToString()
        {
            return string.Format("{0},{1},{2},{3}", Name, Ancestor == null || Ancestor.Name==null ? "null" : Ancestor.Name, Siting, Rank);
        }

        private IEnumerable<string> DecideSiteItems(string rank)
        {
            var result = new List<string>();
            result.Add("Null");
            if (rank != "Both")
            {
                result.Add(rank);
            }
            else
            {
                result.AddRange(new[] {"Column", "Row"});
            }
            if (!result.Contains(Siting))
                Siting = result.FirstOrDefault();

            return result;
        }

        public IEnumerable<string> DecideRanks(string rank)
        {
            if (rank==null)
                return _defaultRanks;
            
            if (rank.ToLower() == "column" || rank.ToLower() == "row") 
                return new[] { rank };

            return _defaultRanks;
        }
    }

    public class ViewModel : ReactiveObject
    {
        private string _Input;
        private string _Output;
        private ViewTagModel _SelectedTag;
        private IList<ViewTagModel> _Tags;

        public ViewModel()
        {
            Tags = new ObservableCollection<ViewTagModel>();
            Save = ReactiveCommand.Create(_ => true, o =>
            {
                if (!Tags.Select(t => t.Id).Contains(SelectedTag.Id))
                {
                    Tags.Add(SelectedTag);
                }
                else
                {
                    //Tags.SingleOrDefault(t => t.Id == SelectedTag.Id) = SelectedTag;
                    foreach (ViewTagModel tag in Tags)
                    {
                        if (tag.Id == SelectedTag.Id)
                        {
                            tag.Name = SelectedTag.Name;
                            tag.Siting = SelectedTag.Siting;
                            tag.Rank = SelectedTag.Rank;
                            tag.Ancestor = SelectedTag.Ancestor;
                            break;
                        }
                    }
                }

                SelectedTag = null;
            });

            Delete = ReactiveCommand.Create(_ => true, o =>
            {
                if (SelectedTag == null)
                    return;
                foreach (ViewTagModel tag in Tags)
                {
                    if (tag.Name == SelectedTag.Name)
                    {
                        Tags.Remove(tag);
                        return;
                    }
                }
            });
            New = ReactiveCommand.Create(_ => true, o => { SelectedTag = new ViewTagModel(Tags); });
            GenTagString = ReactiveCommand.Create(_ => true, o =>
            {
                string tagString = Tags.Aggregate<ViewTagModel, string>(null,
                    (current, tag) => current + (tag.ToString() + ";"));
                Output = tagString;
                //SelectedTag = new ViewTagModel(Tags);
            });

            CreateTags = ReactiveCommand.Create(_ => true, o =>
            {
                IEnumerable<string[]> tagsString = o.ToString().Replace(" ", "").Split(';').Select(s => s.Split(','));
                Tags = new ObservableCollection<ViewTagModel>();
                
                foreach (var stringse in tagsString.Where(tag => tag.Count() > 2))
                {
                    Tags.Add(ViewTagModel.Create(stringse,Tags.ToArray()));
                }
            });
        }

        public string Input
        {
            get { return _Input; }
            set { this.RaiseAndSetIfChanged(o => o.Input, value); }
        }

        public ReactiveCommand CreateTags { get; set; }

        public IList<ViewTagModel> Tags
        {
            get { return _Tags; }
            set { this.RaiseAndSetIfChanged(o => o.Tags, value); }
        }

        public ReactiveCommand Save { get; set; }
        public ReactiveCommand New { get; set; }
        public ReactiveCommand Delete { get; set; }

        public ReactiveCommand GenTagString { get; set; }

        public string Output
        {
            get { return _Output; }
            set { this.RaiseAndSetIfChanged(x => x.Output, value); }
        }

        public ViewTagModel SelectedTag
        {
            get { return _SelectedTag; }
            set { this.RaiseAndSetIfChanged(x => x.SelectedTag, value); }
        }
    }
}